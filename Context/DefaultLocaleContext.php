<?php

declare(strict_types=1);

namespace Ascend\BaseBundle\Context;

use Sylius\Component\Locale\Context\LocaleContextInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

final class DefaultLocaleContext implements LocaleContextInterface
{
    /**
     * @var TokenStorageInterface
     */
    private $defaultLocale;

    /**
     * LocaleContext constructor.
     *
     * @param string $defaultLocale
     */
    public function __construct(string $defaultLocale)
    {
        $this->defaultLocale = $defaultLocale;
    }

    /**
     * {@inheritdoc}
     */
    public function getLocaleCode(): string
    {
        return $this->defaultLocale;
    }
}