<?php

namespace Ascend\BaseBundle\Templating;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class FilterExtension extends AbstractExtension {
    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return [
            new TwigFilter('imagine_filter', [$this, 'filter']),
            new TwigFilter('media_absolute_path', [$this, 'mediaAbsolutePath']),
        ];
    }

    public function mediaAbsolutePath($path)
    {
        return getenv('MEDIA_ABSOLUTE_PATH') . '/' . $path;
    }

    public function filter($path, $filter, array $config = [], $resolver = null)
    {
        return $path;
    }
}