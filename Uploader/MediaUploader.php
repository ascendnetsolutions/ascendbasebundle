<?php

declare(strict_types=1);

namespace Ascend\BaseBundle\Uploader;

use Ascend\BaseBundle\Model\MediaInterface;
use Exception;
use League\Flysystem\FilesystemException;
use League\Flysystem\FilesystemOperator;
use Symfony\Component\HttpFoundation\File\File;
use Webmozart\Assert\Assert;

class MediaUploader implements MediaUploaderInterface
{
    public function __construct(protected FilesystemOperator $filesystem)
    {}

    /**
     * @throws FilesystemException
     * @throws Exception
     */
    public function upload(MediaInterface $media): void
    {
        if (!$media->hasFile()) {
            return;
        }

        $file = $media->getFile();

        /** @var File $file */
        Assert::isInstanceOf($file, File::class);

        if (null !== $media->getPath() && $this->filesystem->has($media->getPath())) {
            $this->remove($media->getPath());
        }

        do {
            $hash = bin2hex(random_bytes(16));
            $fileExtension = $file->guessExtension();

            if (null == $fileExtension) {
                $arr           = explode(".", $file->getClientOriginalName());
                $fileExtension = end($arr);
            }

            $path = $this->expandPath($hash . '.' . $fileExtension);
        } while ($this->isAdBlockingProne($path) || $this->filesystem->has($path));

        $media->setPath($path);

        $this->filesystem->write(
            $media->getPath(),
            file_get_contents($media->getFile()->getPathname())
        );
    }

    /**
     * @throws FilesystemException
     */
    public function remove(string $path): bool
    {
        if ($this->filesystem->has($path)) {
            $this->filesystem->delete($path);
            return true;
        }

        return false;
    }

    private function expandPath(string $path): string
    {
        return sprintf(
            '%s/%s/%s',
            substr($path, 0, 2),
            substr($path, 2, 2),
            substr($path, 4)
        );
    }

    private function isAdBlockingProne(string $path): bool
    {
        return str_contains($path, 'ad');
    }
}
