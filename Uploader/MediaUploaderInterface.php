<?php

declare(strict_types=1);

namespace Ascend\BaseBundle\Uploader;

use Ascend\BaseBundle\Model\MediaInterface;

interface MediaUploaderInterface
{
    public function upload(MediaInterface $media): void;

    public function remove(string $path): bool;
}