<?php

declare(strict_types=1);

namespace Ascend\BaseBundle\Model;

use Doctrine\Common\Collections\Collection;

interface MediaAwareInterface
{
    /**
     * @return Collection|MediaInterface[]
     */
    public function getMedias(): Collection;

    /**
     * @param string $type
     *
     * @return Collection
     */
    public function getMediaByType(string $type): Collection;

    public function hasMedias(): bool;

    public function hasMedia(MediaInterface $media): bool;

    public function addMedia(MediaInterface $media): void;

    public function removeMedia(MediaInterface $media): void;
}
