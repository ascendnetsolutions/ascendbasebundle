<?php

declare(strict_types=1);

namespace Ascend\BaseBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\UploadedFile;

abstract class MediaAware implements MediaAwareInterface
{
    protected $medias;

    public function __construct()
    {
        $this->medias = new ArrayCollection();
    }

    public function removeMedia(MediaInterface $media): void
    {
        if ($this->hasMedia($media)) {
            $media->setOwner(null);
            $this->medias->removeElement($media);
        }
    }

    public function hasMedia(MediaInterface $media): bool
    {
        return $this->medias->contains($media);
    }

    public function getMedias(): Collection
    {
        return $this->medias;
    }

    public function getMediaByType(string $type): Collection
    {
        return $this->medias->filter(
            function (MediaInterface $image) use ($type) {
                return $type === $image->getType();
            }
        );
    }

    public function hasMedias(): bool
    {
        return !$this->medias->isEmpty();
    }

    public function addMedia(MediaInterface $media): void
    {
        if ($media->getFile() instanceof UploadedFile) {
            $media->setOwner($this);
            $this->medias->add($media);
        }
    }
}