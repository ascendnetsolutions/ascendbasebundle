<?php

declare(strict_types=1);

namespace Ascend\BaseBundle\Model;

class AdminUser extends User implements AdminUserInterface
{
    public function __construct()
    {
        $this->roles = [AdminUserInterface::DEFAULT_ADMIN_ROLE];
    }
}