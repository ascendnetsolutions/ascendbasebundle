<?php

declare(strict_types=1);

namespace Ascend\BaseBundle\Model;

interface AdminUserInterface
{
    public const DEFAULT_USER_ROLE = 'ROLE_USER';
    public const DEFAULT_ADMIN_ROLE = 'ROLE_ADMINISTRATION_ACCESS';

}
