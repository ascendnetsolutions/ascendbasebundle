<?php

declare(strict_types=1);

namespace Ascend\BaseBundle\EventListener;

use Ascend\BaseBundle\Model\MediaAwareInterface;
use Ascend\BaseBundle\Uploader\MediaUploaderInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Webmozart\Assert\Assert;

final class MediaUploadListener
{
    /** @var MediaUploaderInterface */
    private $uploader;

    public function __construct(MediaUploaderInterface $uploader)
    {
        $this->uploader = $uploader;
    }

    public function uploadMedias(GenericEvent $event): void
    {
        $subject = $event->getSubject();
        Assert::isInstanceOf($subject, MediaAwareInterface::class);

        $this->uploadSubjectMedias($subject);
    }

    private function uploadSubjectMedias(MediaAwareInterface $subject): void
    {
        $medias = $subject->getMedias();
        foreach ($medias as $media) {
            if ($media->hasFile()) {
                $this->uploader->upload($media);
            }

            // Upload failed? Let's remove that media.
            if (null === $media->getPath()) {
                $medias->removeElement($media);
            }
        }
    }
}
