<?php

declare(strict_types=1);

namespace Ascend\BaseBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

final class AscendBaseBundle extends Bundle
{
}
