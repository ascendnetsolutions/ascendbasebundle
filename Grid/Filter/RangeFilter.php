<?php

namespace Ascend\BaseBundle\Grid\Filter;

use Sylius\Component\Grid\Data\DataSourceInterface;
use Sylius\Component\Grid\Filtering\FilterInterface;

class RangeFilter implements FilterInterface
{
    public function apply(DataSourceInterface $dataSource, string $name, $data, array $options = []): void
    {
        $field = $options['field'] ?? $name;

        $dataSource->restrict($dataSource->getExpressionBuilder()->greaterThanOrEqual($field, $data['min']));
        $dataSource->restrict($dataSource->getExpressionBuilder()->lessThanOrEqual($field, $data['max']));
    }
}