<?php

namespace Ascend\BaseBundle\Grid\Filter;

use Sylius\Component\Grid\Data\DataSourceInterface;
use Sylius\Component\Grid\Filtering\FilterInterface;

final class SelectLikeFilter implements FilterInterface
{
    public function apply(DataSourceInterface $dataSource, string $name, $data, array $options): void
    {
        if (empty($data)) {
            return;
        }

        $field = $options['field'] ?? $name;

        $dataSource->restrict($dataSource->getExpressionBuilder()->like($field, '%' . $data . '%'));
    }
}
