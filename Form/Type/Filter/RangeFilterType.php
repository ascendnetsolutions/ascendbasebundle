<?php

namespace Ascend\BaseBundle\Form\Type\Filter;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RangeFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'min',
            ChoiceType::class,
            [
                'choices'     => array_combine(range($options['range'][0], $options['range'][1]), range($options['range'][0], $options['range'][1])),
                'label' => 'app.ui.min'
            ]
        )->add(
            'max',
            ChoiceType::class,
            [
                'choices'     => array_combine(range($options['range'][0], $options['range'][1]), range($options['range'][0], $options['range'][1])),
                'label' => 'app.ui.max'
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults(
                [
                    'range' => [0, 10],
                ]
            )
            ->setAllowedTypes('range', ['array']);
    }
}