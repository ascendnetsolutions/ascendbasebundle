<?php

declare(strict_types=1);

namespace Ascend\BaseBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;

abstract class MediaType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'type',
                TextType::class,
                [
                    'label'    => 'sylius.form.image.type',
                    'required' => false,
                ]
            )
            ->add(
                'file',
                FileType::class,
                [
                    'label' => 'sylius.form.image.file',
                ]
            );
    }
}
